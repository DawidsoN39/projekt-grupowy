using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{

    private static GameManager INSTANCE;

    public static GameManager Instance 
    {
        get 
        {
            return INSTANCE;
        }
        private set 
        {
        }
    }
    
    private GameManager() 
    {

    }

    private void Awake() 
    {
        if (INSTANCE != null) {
            Destroy(gameObject);
        } else {
            INSTANCE = this;
            DontDestroyOnLoad(gameObject);
        }
    }
}
